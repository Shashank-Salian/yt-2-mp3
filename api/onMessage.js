import downloadMedia from "./ytdownload.js";
import {
	sendMessage,
	editMessage,
	sendVideoFile,
	sendPhoto,
} from "./helpers.js";
import { downloadIgMedia, downloadIgStory, saveStory } from "./igdownload.js";

const onStart = async (msg) => {
	await sendMessage(msg, "Hello, send me the YouTube link to download.");
	await sendMessage(
		msg,
		`Copy the link of the media to download and paste it here👇.`,
		true
	);
};

const onYTLinkRecieved = async (msg) => {
	const sentMsg = await sendMessage(msg, "Downloading...");
	await downloadMedia(msg.text, msg, sentMsg);
	editMessage(sentMsg, "Download complete");
};

const onIGLinkRecieved = async (msg) => {
	const sentMsg = await sendMessage(msg, "Downloading...");
	await downloadIgMedia(msg.text, msg, sentMsg);
	editMessage(sentMsg, "Download complete");
};

const onStoryDl = async (msg) => {
	const sentMsg = await sendMessage(msg, "Getting stories...");
	const username = msg.text.split(" ")[1];
	await downloadIgStory(username, msg, sentMsg);
};

const onBtnPress = async (data, msg) => {
	console.log(global.__storiesInfo__);
	await editMessage(msg, "Just a sec...");
	const query = data.split("-")[1];

	if (query === "all") {
		for (let link of global.__storiesInfo__.downloadLinks) {
			const video = link.mediaType === "video";
			const path = await saveStory(link.url, `${Date.now()}`, video);

			if (video) {
				await sendVideoFile(msg, path);
			} else {
				await sendPhoto(msg, path);
			}
		}
		return;
	}
	const i = parseInt(query);
	const isVideo = global.__storiesInfo__.downloadLinks[i].mediaType === "video";
	const dest = await saveStory(
		global.__storiesInfo__.downloadLinks[i].url,
		`${Date.now()}`,
		isVideo
	);

	if (isVideo) {
		await sendVideoFile(msg, dest);
		return;
	}
	await sendPhoto(msg, dest);
	return;
};

export { onStart, onYTLinkRecieved, onIGLinkRecieved, onBtnPress, onStoryDl };
