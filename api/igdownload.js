import fs from "fs";
import https from "https";
import {
	getShortcode,
	sendPhoto,
	sendVideoFile,
	editMessage,
} from "./helpers.js";

const getPostInfo = (url) => {
	return new Promise((resolve, reject) => {
		https
			.get(
				{
					method: "GET",
					hostname: "instagram47.p.rapidapi.com",
					port: null,
					path: `/post_details?shortcode=${getShortcode(url)}`,
					headers: {
						"x-rapidapi-host": "instagram47.p.rapidapi.com",
						"x-rapidapi-key": process.env.API_TOKEN,
						useQueryString: true,
					},
				},
				(res) => {
					let data = "";
					res.on("data", (chunk) => {
						data += chunk.toString();
					});

					res.on("end", () => {
						data = JSON.parse(data);
						resolve(data);
					});
				}
			)
			.on("error", (err) => {
				reject(err);
			});
	});
};

const getUserId = (username) => {
	return new Promise((resolve, reject) => {
		https
			.get(
				{
					method: "GET",
					hostname: "instagram-stories1.p.rapidapi.com",
					port: null,
					path: `/v1/get_user_id?username=${username}`,
					headers: {
						"x-rapidapi-host": "instagram-stories1.p.rapidapi.com",
						"x-rapidapi-key": process.env.API_TOKEN,
						useQueryString: true,
					},
				},
				(res) => {
					let data = "";

					res.on("data", (chunk) => {
						data += chunk.toString();
					});

					res.on("end", () => {
						data = JSON.parse(data);
						resolve(data);
					});
				}
			)
			.on("error", (err) => {
				console.log(err);
				reject(err);
			});
	});
};

const getUserStories = (userid) => {
	return new Promise((resolve, reject) => {
		https
			.get(
				{
					method: "GET",
					hostname: "instagram-stories1.p.rapidapi.com",
					port: null,
					path: `/v2/user_stories?userid=${userid}`,
					headers: {
						"x-rapidapi-host": "instagram-stories1.p.rapidapi.com",
						"x-rapidapi-key": process.env.API_TOKEN,
						useQueryString: true,
					},
				},
				(res) => {
					let data = "";

					res.on("data", (chunk) => {
						data += chunk.toString();
					});

					res.on("end", () => {
						data = JSON.parse(data);
						resolve(data);
					});
				}
			)
			.on("error", (err) => {
				console.log(err);
				reject(err);
			});
	});
};

const saveStory = (url, title, isVideo) => {
	return new Promise((resolve, reject) => {
		https.get(url, (res) => {
			let data = [];

			res.on("data", (chunk) => {
				data.push(Buffer.from(chunk));
			});

			res.on("end", () => {
				const dest = `/tmp/${title}.${isVideo ? ".mp4" : ".jpg"}`;
				const writeStream = fs.createWriteStream(dest);

				data = Buffer.concat(data);

				writeStream.write(data, (err) => {
					if (!err) {
						resolve(dest);
					}
					reject(err);
				});
			});
		});
	});
};

const saveMedia = (url, title, isVideo) => {
	return new Promise((resolve, reject) => {
		https
			.get(url, (res) => {
				let data = [];

				res.on("data", (chunk) => {
					data.push(Buffer.from(chunk));
				});

				res.on("end", () => {
					const dest = `/tmp/${title}.${isVideo ? ".mp4" : ".jpg"}`;
					const writeStream = fs.createWriteStream(dest);

					data = Buffer.concat(data);

					writeStream.write(data, (err) => {
						if (!err) {
							resolve(dest);
						}
						reject(err);
					});
				});
			})
			.on("error", (err) => {
				reject(err);
			});
	});
};

const downloadIgStory = async (username, msg, errMsg) => {
	try {
		const userInfo = await getUserId(username);

		const storiesInfo = await getUserStories(userInfo.user_id);

		global.__storiesInfo__ = storiesInfo;

		if (storiesInfo?.totalStories === 1) {
			const isVideo = storiesInfo.downloadLinks[0].mediaType === "video";
			const dest = await saveStory(
				storiesInfo.downloadLinks[0].url,
				`${username}_story_1`,
				isVideo
			);

			if (isVideo) {
				await sendVideoFile(msg, dest);
				return;
			}
			await sendPhoto(msg, dest);
			return;
		}

		const keys = [];
		let btn = [{ text: `All`, callback_data: "dlStory-all" }];
		storiesInfo?.downloadLinks.forEach((_, i) => {
			btn.push({
				text: `${i + 1}${
					i + 1 === 1 ? "ˢᵗ" : i + 1 === 2 ? "ⁿᵈ" : i + 1 === 3 ? "ʳᵈ" : "ᵗʰ"
				}`,
				callback_data: `dlStory-${i}`,
			});
			if (btn.length === 3) {
				keys.push([...btn]);
				btn.length = 0;
			}
			if (i === storiesInfo.downloadLinks.length - 1 && btn.length) {
				keys.push([...btn]);
			}
		});
		await bot.sendMessage(
			msg.chat.id,
			"Choose what stories you want to download from this account ?",
			{
				reply_to_message_id: msg.message_id,
				reply_markup: {
					inline_keyboard: keys,
				},
			}
		);
	} catch (err) {
		console.log(err);
		editMessage(errMsg, "Some error occured");
	}
};

const downloadIgMedia = async (url, msg, errMsg) => {
	try {
		const info = await getPostInfo(url);
		if (info.body.is_video) {
			const path = await saveMedia(
				info.body.video_url,
				info.body.shortcode,
				true
			);
			await sendVideoFile(msg, path);
		} else {
			const path = await saveMedia(
				info.body.display_url,
				info.body.shortcode,
				false
			);
			await sendPhoto(msg, path);
		}
	} catch (err) {
		console.log(err);
		if (err.statusCode === 105)
			editMessage(
				errMsg,
				"I guess the limit has exceeded.\nplease try again after some time...\n\nor tomorrow :|"
			);
		else if (err.statusCode === 104)
			editMessage(
				errMsg,
				"Maybe the link is wrong, or is not supported, or please try after some time."
			);
		else editMessage(errMsg, "Some error occured");
	}
};

export { downloadIgMedia, downloadIgStory, saveStory };
