import "./initBot.js";
import {
	onYTLinkRecieved,
	onStart,
	onIGLinkRecieved,
	onBtnPress,
	onStoryDl,
} from "./onMessage.js";
import { sendMessage, removeUpdates } from "./helpers.js";

module.exports = async (req, res) => {
	try {
		if (req.body && req.body.message) {
			const msg = { ...req.body.message };
			const text = msg.text;
			if (
				msg.from.id === parseInt(process.env.ALLOWED1) ||
				msg.from.id === parseInt(process.env.ALLOWED2) ||
				msg.from.id === parseInt(process.env.ALLOWED3) ||
				msg.from.id === parseInt(process.env.ALLOWED4)
			) {
				if (text === "/start") await onStart(msg);
				else if (text === "/stop") await removeUpdates();
				else if (
					/(http(s)?:\/\/)?(m\.)?(www\.)?(youtube\.com)\/(watch)(\/)?(\?)(.*)(v=)(.*[^\s])$/.test(
						text
					) ||
					/(http(s)?:\/\/)?(youtu\.be)\/(.*)$/.test(text)
				)
					await onYTLinkRecieved(msg);
				else if (
					/(http(s)?:\/\/)?(www\.)?instagram\.com\/(p|reel|tv)\/.*$/.test(text)
				)
					await onIGLinkRecieved(msg);
				else if (text.startsWith("/story")) await onStoryDl(msg);
				else await sendMessage(msg, "Send a valid YouTube/Instagram link.");
			} else {
				await sendMessage(msg, "Sorry, this service is limited : (");
			}
		} else if (
			req.body &&
			req.body.callback_query &&
			req.body.callback_query.message.reply_markup.inline_keyboard
		) {
			const msg = { ...req.body.callback_query.message };
			if (
				msg.chat.id === parseInt(process.env.ALLOWED1) ||
				msg.chat.id === parseInt(process.env.ALLOWED2)
			) {
				await onBtnPress(req.body.callback_query.data, msg);
			}
		}
	} catch (err) {
		console.log("Error :");
		console.log(err);
	}
	res.status(200).send("OK");
};
