const sendMessage = async (msg, text, noReply = false) => {
	if (!noReply) {
		const sentMsg = await bot.sendMessage(msg.chat.id, text, {
			reply_to_message_id: msg.message_id,
			parse_mode: "HTML",
		});
		return sentMsg;
	}
	const sentMsg = await bot.sendMessage(msg.chat.id, text, {
		parse_mode: "HTML",
	});
	return sentMsg;
};

const editMessage = async (msg, textToReplace) => {
	const editedMessage = await bot.editMessageText(textToReplace, {
		chat_id: msg.chat.id,
		message_id: msg.message_id,
	});
	return editedMessage;
};

const getVideoId = (url) => {
	let id = "dQw4w9WgXcQ";
	if (
		/(http(s)?:\/\/)?(m\.)?(www\.)?(youtube\.com)\/(watch)(\/)?(\?)(.*)(v=)(.*[^\s])$/.test(
			url
		)
	) {
		id = url.split("v=")[1].split("&")[0];
	} else if (/(http(s)?:\/\/)?(youtu\.be)\/(.*)$/.test(url)) {
		id = url.split(".be/")[1].split("?")[0];
	}
	return id;
};

const sendVideoBuffer = async (msg, buff) => {
	bot.sendChatAction(msg.chat.id, "upload_video");
	const sentMsg = await bot.sendVideo(msg.chat.id, buff);
	return sentMsg;
};

const sendVideoFile = async (msg, path) => {
	bot.sendChatAction(msg.chat.id, "upload_video");
	const sentMsg = await bot.sendVideo(msg.chat.id, path);
	return sentMsg;
};

const sendAudioFile = async (msg, path) => {
	bot.sendChatAction(msg.chat.id, "upload_audio");
	const sentMsg = await bot.sendAudio(msg.chat.id, path);
	return sentMsg;
};

const sendPhoto = async (msg, path) => {
	bot.sendChatAction(msg.chat.id, "upload_photo");
	const sentMsg = await bot.sendPhoto(msg.chat.id, path);
	return sentMsg;
};

const getShortcode = (url) => {
	if (url.startsWith("https://")) {
		return url.split("/")[4];
	}
	return url.split("/")[2];
};

const removeUpdates = async () => {
	await bot.deleteWebhook({ drop_pending_updates: true });
	const updates = await bot.getUpdates();
	await bot
		.getUpdates({ offset: updates[updates.length - 1].update_id + 1 })
		.catch((err) => {
			console.log(err);
		});
	await bot.setWebhook({ url: "https://yt-2-mp3.vercel.app/api/webhook" });
};

export {
	sendMessage,
	getVideoId,
	editMessage,
	sendVideoBuffer,
	sendAudioFile,
	getShortcode,
	sendVideoFile,
	sendPhoto,
	removeUpdates,
};
