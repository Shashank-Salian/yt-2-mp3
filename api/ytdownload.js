import https from "https";
import { editMessage, getVideoId, sendAudioFile } from "./helpers.js";

const getVideoInfo = (url) => {
	return new Promise((resolve, reject) => {
		https
			.get(
				{
					method: "GET",
					hostname: "youtube-mp36.p.rapidapi.com",
					port: null,
					path: `/dl?id=${getVideoId(url)}`,
					headers: {
						"x-rapidapi-host": "youtube-mp36.p.rapidapi.com",
						"x-rapidapi-key": process.env.API_TOKEN,
						useQueryString: true,
					},
				},
				(res) => {
					let data = "";
					res.on("data", (chunk) => {
						data += chunk.toString();
					});
					res.on("end", () => {
						data = JSON.parse(data);
						resolve(data);
					});
				}
			)
			.on("error", (err) => {
				console.log(err);
				reject(err);
			});
	});
};

const downloadMedia = async (url, msg, errMsg) => {
	try {
		const info = await getVideoInfo(url);
		await sendAudioFile(msg, info.link);
	} catch (err) {
		console.log(err);
		editMessage(errMsg, "Some error occured");
	}
};

export default downloadMedia;
